﻿using MatrixLib.Matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodowanie.Task2
{
    public class BitInputMatrix : MatrixBase<string, string>
    {
        #region Constructor
        public BitInputMatrix(string binaryInput, IList<int> pow2Indexes)
        {
            this.binaryInput = binaryInput;
            this.pow2Indexes = pow2Indexes;
            var columns = new List<string>();

            int index = 1;
            foreach (var item in binaryInput)
            {
                while (pow2Indexes.Contains(index))
                {
                    index++;
                }
                columns.Add("d" + index);
                index++;
            }

            bites = columns.ToArray();


            _rowHeaderToValueProviderMap = new Dictionary<string, CellValueProvider>();
            this.PopulateCellValueProviderMap();
        }

        #endregion // Constructor

        #region Base Class Overrides

        protected override IEnumerable<string> GetColumnHeaderValues()
        {
            return bites;
        }

        protected override IEnumerable<string> GetRowHeaderValues()
        {
            return _rowHeaderToValueProviderMap.Keys;
        }

        protected override object GetCellValue(string rowHeaderValue, string columnHeaderValue)
        {
            return _rowHeaderToValueProviderMap[rowHeaderValue](columnHeaderValue);
        }

        #endregion // Base Class Overrides

        #region Private Helpers

        void PopulateCellValueProviderMap()
        {
            int index = 0;
            _rowHeaderToValueProviderMap.Add(
            "", x =>
                             {
                                 string cellValue = binaryInput[index].ToString();
                                 index++;
                                 return cellValue;
                             });
        }

        #endregion // Private Helpers

        #region Fields

        readonly string binaryInput;
        readonly string[] bites;
        readonly IList<int> pow2Indexes;
        readonly int rows;
        readonly Dictionary<string, CellValueProvider> _rowHeaderToValueProviderMap;

        /// <summary>
        /// This delegate type describes the signature of a method 
        /// used to produce the value of a cell in the matrix.
        /// </summary>
        private delegate object CellValueProvider(string value);

        #endregion // Fields
    }
}

﻿using Kodowanie.Task1and3;
using Kodowanie.Task2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kodowanie.Extenders;

namespace Kodowanie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Paragraph paragraph;
        public MainWindow()
        {
            InitializeComponent();
        }

        string task2InputOldValue = String.Empty;

        private void GenerateRelations_Click(object sender, RoutedEventArgs e)
        {
            paragraph = new Paragraph();
            int informationLength = 0;

            if (!int.TryParse(CodeBlockTotalLength.Text, out informationLength) || informationLength <= 0)
            {
                MessageBox.Show("Wprowadzona wartość jest nieprawidłowa", "Błąd wartości wejściowej", MessageBoxButton.OK, MessageBoxImage.Warning);
                CodeBlockTotalLength.Text = "0";
                return;
            }

            var pow2Indexes = HMatrixHelper.GetRowsCount(informationLength);
            HMatrixControl.DataContext = new HMatrix(informationLength + pow2Indexes.Count, pow2Indexes);

            CreateTask1Result(pow2Indexes, informationLength + pow2Indexes.Count, "w", "w", "#0B450F");
            RelationsTextBlock.Document = new FlowDocument(paragraph);
            RelationsTextBlock.Visibility = System.Windows.Visibility.Visible;
        }

        private void CreateTask1Result(IList<int> pow2Indexes, int totalMessageLength, string paritySign, string informationSign, string parityColor = "#000000", string informationColor = "#000000")
        {
            foreach (var i in pow2Indexes)
            {
                if (i != 1)
                {
                    paragraph.Inlines.Add(new LineBreak());
                }
                paragraph.Inlines.Add(new Bold(new Run(paritySign + i))
                {
                    Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom(parityColor))
                });
                paragraph.Inlines.Add(" = ");
                bool newElement = true;
                for (int j = 1; j <= totalMessageLength; j++)
                {
                    var currentValueToReturn = HMatrixHelper.GetCellValueForPow2Index(i, j.ToString(), totalMessageLength);

                    if (currentValueToReturn == "1")
                    {
                        if (i != j)
                        {
                            if (!newElement)
                                paragraph.Inlines.Add(" ⊕ ");

                            newElement = false;
                            paragraph.Inlines.Add(new Bold(new Run(informationSign + j))
                            {
                                Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom(informationColor))
                            });
                        }
                    }
                }
            }
        }

        private void bTaskInfo_Click(object sender, RoutedEventArgs e)
        {
            var taskInfo = new TaskInfo();
            taskInfo.Show();
        }

        private void InputToCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            var control = (TextBox)sender;
            string input = control.Text;
            if (String.IsNullOrWhiteSpace(input))
            {
                BinaryInput.Text = String.Empty;
                cbInputType.IsEnabled = String.IsNullOrWhiteSpace(input);
                GenerateCode.IsEnabled = !String.IsNullOrWhiteSpace(input);
                task2InputOldValue = String.Empty;
                return;
            }
            if (cbInputType.SelectedItem == null)
            {
                MessageBox.Show("Należy określić typ wartości wejściowej", "Błąd wartości wejściowej", MessageBoxButton.OK, MessageBoxImage.Warning);
                control.Text = String.Empty;
                return;
            }

            cbInputType.IsEnabled = String.IsNullOrWhiteSpace(input);
            GenerateCode.IsEnabled = !String.IsNullOrWhiteSpace(input);

            if (cbInputType.SelectionBoxItem.ToString() == "Ciąg znaków")
            {
                byte[] inputByte = System.Text.Encoding.ASCII.GetBytes(input);
                input = String.Empty;
                foreach (var item in inputByte)
                {
                    input += Convert.ToString((int)item, 2).PadLeft(8, '0');
                }
            }
            else if (cbInputType.SelectionBoxItem.ToString() == "Liczba")
            {
                long intInput = 0;

                if (long.TryParse(input, out intInput))
                {
                    input = Convert.ToString(intInput, 2);
                }
                else
                {
                    MessageBox.Show("Podany zank nie jest liczbą", "Błąd wartości wejściowej", MessageBoxButton.OK, MessageBoxImage.Warning);
                    control.Text = task2InputOldValue;
                    return;
                }
            }
            else
            {
                if (input.Any(x => (int)x != 48 && (int)x != 49))
                {
                    MessageBox.Show("Należy podać 0 lub 1", "Błąd wartości wejściowej", MessageBoxButton.OK, MessageBoxImage.Warning);
                    control.Text = task2InputOldValue;
                    return;
                }
            }

            BinaryInput.Clear();

            task2InputOldValue = control.Text;
            BinaryInput.Text = input;
        }

        private void GenerateCode_Click(object sender, RoutedEventArgs e)
        {
            paragraph = new Paragraph();
            var pow2Indexes = HMatrixHelper.GetRowsCount(BinaryInput.Text.Length);
            BitInputMatrixControl.DataContext = new BitInputMatrix(BinaryInput.Text, pow2Indexes);

            CreateTask1Result(pow2Indexes, BinaryInput.Text.Length + pow2Indexes.Count, "p", "d", "#0A3ECD", "#169F00");
            AssumptionsRTB.Document = new FlowDocument(paragraph);

            paragraph = new Paragraph();

            var result = GenerateHamming(BinaryInput.Text, pow2Indexes, "#0A3ECD", "#169F00");
            CalculationHammingRTB.Document = new FlowDocument(paragraph);

            paragraph = new Paragraph();
            GenerateTask2ResltInColor(result, pow2Indexes, "#7697F3", "#83FD70");
            Task2ResultRTB.Document = new FlowDocument(paragraph);
        }

        private string GenerateHamming(string codeBlock, IList<int> pow2Indexes, string parityColor = "#000000", string informationColor = "#000000")
        {
            Dictionary<int, string> informationIndexesMapped = new Dictionary<int, string>();

            int index = 1;
            foreach (var item in codeBlock)
            {
                while (pow2Indexes.Contains(index))
                {
                    index++;
                }
                informationIndexesMapped.Add(index, item.ToString());
                index++;
            }


            Dictionary<int, string> listOfIndexValues = new Dictionary<int, string>();
            pow2Indexes.ToList().ForEach(x => listOfIndexValues.Add(x, "0"));
            int totalMessageLength = codeBlock.Length + listOfIndexValues.Count;

            foreach (var i in pow2Indexes)
            {
                bool newElement = true;
                var tmpParagraph = new List<Inline>();
                int countOf1 = 0;
                for (int j = 1; j <= totalMessageLength; j++)
                {
                    var currentValueToReturn = HMatrixHelper.GetCellValueForPow2Index(i, j.ToString(), totalMessageLength);

                    if (currentValueToReturn == "1")
                    {
                        if (i != j)
                        {
                            if (!newElement)
                                tmpParagraph.Add(new Run(" ⊕ "));

                            newElement = false;

                            string informationValue = informationIndexesMapped[j];
                            tmpParagraph.Add(new Bold(new Run(informationValue))
                            {
                                Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom(informationColor))
                            });

                            if (informationValue == "1")
                            {
                                countOf1++;
                            }
                        }
                    }
                }
                if (i != 1)
                {
                    paragraph.Inlines.Add(new LineBreak());
                }
                paragraph.Inlines.Add(new Bold(new Run((countOf1 % 2).ToString()))
                {
                    Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom(parityColor))
                });
                paragraph.Inlines.Add(" = ");
                paragraph.Inlines.AddRange(tmpParagraph);

                listOfIndexValues[i] = (countOf1 % 2).ToString();
            }

            string result = "";

            for (int i = 1; i <= totalMessageLength; i++)
            {
                if (pow2Indexes.Contains(i))
                    result += listOfIndexValues[i];
                else
                    result += informationIndexesMapped[i];
            }
            return result;
        }

        private void GenerateTask2ResltInColor(string result, IList<int> pow2Indexes, string parityColor = "#000000", string informationColor = "#000000")
        {
            for (int i = 0; i < result.Length; i++)
            {
                if (pow2Indexes.Contains(i + 1))
                {
                    paragraph.Inlines.Add(new Bold(new Run(result[i].ToString()))
                    {
                        Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(parityColor))
                    });
                }
                else
                {
                    paragraph.Inlines.Add(new Bold(new Run(result[i].ToString()))
                    {
                        Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(informationColor))
                    });
                }
            }
        }

        private string HammingCheck(string codeBlock, out string validationMessage)
        {
            //stworzenie listy potęg liczby 2
            var pow2Index = HMatrixHelper.GetRowsCount(codeBlock.Length);

            int index = 0;
            //Utworzenie tablicy z binarnymi wartościami indexów jedynek występujących w bloku kodowym
            int codeBlockLength = codeBlock.Length;
            int countOfHammingBits = pow2Index.Count;

            string[] valuesForPowOf2 = new string[countOfHammingBits];

            List<string> valuesOf1 = new List<string>();

            for (int i = 0; i < codeBlockLength; i++)
            {
                if (codeBlock[i] == '1')
                {
                    valuesOf1.Add(Convert.ToString((i + 1), 2).PadLeft(countOfHammingBits, '0'));
                }
            }

            //Liczenie wartości dla każdego bitu o indexie będącym potęgą liczby 2
            foreach (var item in valuesOf1)
            {
                for (int j = 0; j < item.Length; j++)
                {
                    if (j < valuesForPowOf2.Length)
                    {
                        valuesForPowOf2[j] += item[j].ToString();
                    }
                }
            }

            List<int> pow2FinalValue = new List<int>();

            foreach (string item in valuesForPowOf2)
            {
                index = 0;
                foreach (var _char in item)
                {
                    if (_char.ToString() == "1")
                    {
                        index++;
                    }
                }

                pow2FinalValue.Add(index % 2);
            }

            string syndrom = String.Join("", pow2FinalValue);
            long result = Convert.ToInt64(syndrom, 2);
            index = 0;
            if (result == 0)
            {
                validationMessage = "Test Hamminga: Ciąg wejściowy przeszedł test.";
            }
            else
            {
                validationMessage = "Test Hamminga: Ciąg wejściowy posiada błąd w bicie na pozycji: " + result;
            }
            return syndrom;
        }

        private void CheckResult_Click(object sender, RoutedEventArgs e)
        {
            string encodedInput = new TextRange(Task2ResultRTB.Document.ContentStart, Task2ResultRTB.Document.ContentEnd).Text;
            string checkMessage;
            task2SyndromControl.Text = HammingCheck(encodedInput, out checkMessage);

            int wrongBitPossition = Convert.ToInt32(task2SyndromControl.Text, 2);
            if (wrongBitPossition != 0)
            {
                Task2ResultRTB.Select(wrongBitPossition, 1, Colors.Red);
            }

            MessageBox.Show(checkMessage, "Sprawdzenie wygenerowanej informacji", MessageBoxButton.OK, MessageBoxImage.Information);
        }

    }
}

﻿using MatrixLib.Matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodowanie
{
    public class HMatrix : MatrixBase<string, string>
    {
        #region Constructor
        public HMatrix(int columnsCount, IList<int> powIndexes)
        {
            var columns = new List<string>();
            for (int i = 1; i <= columnsCount; i++)
            {
                columns.Add(i.ToString());
            }

            bites = columns.ToArray();

            pow2Indexes = powIndexes;

            _rowHeaderToValueProviderMap = new Dictionary<string, CellValueProvider>();
            this.PopulateCellValueProviderMap();
        }

        #endregion // Constructor

        #region Base Class Overrides

        protected override IEnumerable<string> GetColumnHeaderValues()
        {
            return bites;
        }

        protected override IEnumerable<string> GetRowHeaderValues()
        {
            return _rowHeaderToValueProviderMap.Keys;
        }

        protected override object GetCellValue(string rowHeaderValue, string columnHeaderValue)
        {
            return _rowHeaderToValueProviderMap[rowHeaderValue](columnHeaderValue);
        }

        #endregion // Base Class Overrides

        #region Private Helpers

        void PopulateCellValueProviderMap()
        {
            String builder = String.Empty;
            foreach (var item in pow2Indexes)
            {
                _rowHeaderToValueProviderMap.Add(
                "p" + item, x =>
                                 {
                                     return HMatrixHelper.GetCellValueForPow2Index(item, x, bites.Length);
                                 });
            }
        }

        #endregion // Private Helpers

        #region Fields

        readonly string[] bites;
        readonly IList<int> pow2Indexes;
        readonly int rows;
        readonly Dictionary<string, CellValueProvider> _rowHeaderToValueProviderMap;

        /// <summary>
        /// This delegate type describes the signature of a method 
        /// used to produce the value of a cell in the matrix.
        /// </summary>
        private delegate object CellValueProvider(string value);

        #endregion // Fields
    }
}

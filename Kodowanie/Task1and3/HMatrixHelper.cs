﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodowanie
{
    public class HMatrixHelper
    {
        public static string RelationsString { get; set; }

        public static string GetCellValueForPow2Index(int powIndex, string currentBit, int bitesCount)
        {
            int bitIndex = int.Parse(currentBit);
            if (powIndex > bitIndex)
            {
                return "0";
            }
            else if (powIndex == bitIndex)
            {
                return "1";
            }
            else
            {
                var evenIndexes = new List<int>();
                bool _break = false;

                for (int i = powIndex; i <= bitesCount; i = i + powIndex)
                {
                    if (!_break)
                    {
                        evenIndexes.Add(i);

                        for (int j = 1; j < powIndex; j++)
                        {
                            evenIndexes.Add(i + j);
                        }

                        _break = true;
                    }
                    else
                    {
                        _break = false;
                    }
                }

                return evenIndexes.Contains(bitIndex) ? "1" : "0";
            }
        }

        public static List<int> GetRowsCount(int totalMessageLength)
        {
            List<int> indexes = new List<int>();
            for (int i = 1; i <= totalMessageLength; i++)
            {
                if ((i & (i - 1)) == 0)
                {
                    indexes.Add(i);
                }
            }
            return indexes;
        }
    }
}
